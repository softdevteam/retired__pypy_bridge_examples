# Benchmarks to run and a parameter.
BENCHMARKS = {
        "fannkuch" : 11,
        "gcbench" : 3,
        "richards" : 1000,
}

# VMs to benchmarks and their capabilities.
VMS = {
        #"/usr/bin/php" : {
        #    "name" :      "Zend",
        #    "variants" :  ["mono-php"],
        #},
        "/usr/bin/hhvm" : {
            "name" :      "HHVM",
            "variants" :  ["mono-php"],
        },
        "/home/vext01/research/hippyvm.hippyvm/hippy-c-mono" : {
            "name" :        "HippyVM",
            "variants" :    ["mono-php"],
        },
        "/home/vext01/research/hippyvm.hippyvm/hippy-c-comp" : {
            "name" :        "PyHyp",
            "variants" :    ["mono-php", "comp"],
        },
}

# Repetitions at each level
N_EXECUTIONS = 1
N_ITERATIONS = 5

OUT_FILE = "one_exec2.json"
