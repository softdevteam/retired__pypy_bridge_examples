<?php

$src = <<<EOD
def fib(n):
	if n == 0: return 0
	if n == 1: return 1
	return fib(n-1) + fib(n-2)
EOD;

$m = embed_py_mod("fib", $src);

for ($i = 0; $i < 10; $i++) {
	echo($m->fib($i) . "\n");
}

?>
