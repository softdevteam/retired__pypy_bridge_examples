<?php

$reps = intval($argv[1]);

$src = <<<EOD
def mystone():
	from test import pystone
	pystone.main($reps);
EOD;

pypy_new_func($src);
mystone();

?>
