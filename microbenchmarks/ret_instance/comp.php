<?php

class A {
}

$pysrc = <<<EOD
def ret_inst(x):
	return x;
EOD;
embed_py_func($pysrc);

$reps = $argv[1];

$a = new A();
for ($i = 0; $i < $reps; $i++) {
	ret_inst($a);
}

?>
