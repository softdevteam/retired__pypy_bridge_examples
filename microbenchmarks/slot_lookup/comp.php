<?php

class C {
    public $x;
    function __construct() {
        $this->x = 1;
    }
} 

$pysrc = <<<EOD
def lookup_slot(x):
    return x.x
EOD;
embed_py_func($pysrc);

$a = new C();
for ($i = 0; $i < $argv[1]; $i++) {
	$b = lookup_slot($a);
}
echo($b);
?>
