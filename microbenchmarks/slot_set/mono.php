<?php

class C {
    public $x;
    function __construct() {
        $this->x = 1;
    }
} 

function lookup_slot($x) {
	$x->x = 2;
}

$a = new C();
for ($i = 0; $i < $argv[1]; $i++) {
	lookup_slot($a);
}
?>
