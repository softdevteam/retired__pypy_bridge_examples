<?php

$pysrc = <<<EOD
def return_list(l):
	return l;
EOD;
embed_py_func($pysrc);

$reps = $argv[1];

// build a big list
$ar = array();
for ($i = 0; $i < pow(2, 16); $i++) {
	$ar[] = $i;
}

for ($i = 0; $i < $reps; $i++) {
	return_list($ar);
}

?>
