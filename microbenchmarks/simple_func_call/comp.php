<?php

$pysrc = "def f(): pass";
$f = embed_py_func($pysrc);

function run_exec($reps) {
    global $f;
	for ($i = 0; $i < $reps; $i++) {
		$f();
	}
}

?>
